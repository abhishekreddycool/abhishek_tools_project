﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.PackageManager;
using static GameObjectsEditor;


public class Item : ScriptableObject
{
    public string Name;
    public float Health;
    public Sprite texture;
    public Colliders collider;
    public float Points;
    public Item(string name, float health, Colliders coll, Sprite text, float points)
    {
        Name = name;
        Health = health;
        texture = text;
        collider = coll;
        Points = points;
    }
}
