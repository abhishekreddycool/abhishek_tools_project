﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class GameObjectsEditor : EditorWindow
{
    //Toolbar data
    int toolBar = 0;
    string[] objectNames = new string[]
    {
        "Player",
        "Enemy",
        "Spawn",
        "Items",
        "Start",
        "End"
    };

    public enum Colliders
    {
        CircleColiider,
        BoxCollider
    };

    //Player data
    string playerName;
    float playerHealth;
    Colliders playerCollider;
    Sprite playerTexture;

    //Enemy Data
    private string enemyName;
    private float enemyHealth;
    Colliders enemyCollider;
    private Sprite enemyTexture;

    //Item Data
    private string itemName;
    private float itemHealth;
    private float itemPoints;
    Colliders itemCollider;
    private Sprite itemTexture;

    //Spawner data
    private string spawnName;
    private float spawnHealth;
    private float spawnTime;
    Colliders spawnCollider;
    private Enemy spawnEnemy;
    private Sprite spawnEnemyTexture;
    private Sprite spawnTexture;

    //End Data
    private string finishName;
    Colliders endCollider;
    private Sprite finishTexture;

    //Start Data
    private string startName;
    Colliders startCollider;
    private Sprite startTexture;

    //creating a mennu item
    [MenuItem("Tools/GameObjects Editor")]
    public static void getWindow()
    {
        //Generating a Window
        EditorWindow.GetWindow<GameObjectsEditor>();
    }
    //Setting the Window Name and Dimensions on window Creation
    public GameObjectsEditor()
    {
        this.titleContent = new GUIContent("Game Objects");
        this.minSize = new Vector2(380, 380);
        this.maxSize = new Vector2(380, 380);
    }

    public void OnGUI()
    {
        //creating a toolbr
        toolBar = GUILayout.Toolbar(toolBar, objectNames);
        // when first tab is selected in the tool bar
        if (toolBar==0)
        {
            //fields for the player data
            playerName = EditorGUILayout.TextField("Name:",playerName);
            playerHealth = EditorGUILayout.Slider("Health", playerHealth,0,100);
            playerCollider = (Colliders)EditorGUILayout.EnumPopup("Collider", playerCollider);
            playerTexture = (Sprite)EditorGUILayout.ObjectField("Player:", playerTexture, typeof(Sprite), true,GUILayout.MaxWidth(200));
            if(GUILayout.Button("Add Player"))
            {
                Player player = ScriptableObject.CreateInstance<Player>();
                player = new Player(playerName, playerHealth, playerCollider,playerTexture);
                AssetDatabase.CreateAsset(player, "Assets/Resources/GameObjects/Player.asset");
                playerName = null;
                playerHealth = 0;
                playerTexture = null;
            }
        }
        // when second tab is selected in the tool bar
        if (toolBar == 1)
        {
            //fields for the enemy data
            enemyName = EditorGUILayout.TextField("Name:", enemyName);
            enemyHealth = EditorGUILayout.Slider("Health", enemyHealth, 0, 100);
            enemyCollider= (Colliders)EditorGUILayout.EnumPopup("Collider", enemyCollider);
            enemyTexture = (Sprite)EditorGUILayout.ObjectField("Enemy:", enemyTexture, typeof(Sprite), true, GUILayout.MaxWidth(200));
            if (GUILayout.Button("Add Enemy"))
            {
                Enemy enemy = ScriptableObject.CreateInstance<Enemy>();
                enemy = new Enemy(enemyName, enemyHealth, enemyCollider, enemyTexture);
                AssetDatabase.CreateAsset(enemy, ("Assets/Resources/GameObjects/" + enemyName)+".asset");
                enemyName = null;
                enemyHealth = 0;
                enemyTexture = null;
            }
        }
        // when third tab is selected in the tool bar
        if (toolBar == 2)
        {
            //fields for the spawn data
            spawnName = EditorGUILayout.TextField("Name:",  spawnName);
            spawnHealth = EditorGUILayout.Slider("Health", spawnHealth, 0, 100);
            spawnTime = EditorGUILayout.Slider("Time", spawnTime, 0, 10);
            spawnCollider = (Colliders)EditorGUILayout.EnumPopup("Collider", spawnCollider);
            spawnTexture = (Sprite)EditorGUILayout.ObjectField("Spawn:", spawnTexture, typeof(Sprite), true, GUILayout.MaxWidth(200));
            spawnEnemy = (Enemy)EditorGUILayout.ObjectField("SpawnEnemy:", spawnEnemy, typeof(Enemy), true, GUILayout.MaxWidth(300));
            
            if (GUILayout.Button("Add Spawner"))
            {
                Spawn spawn = ScriptableObject.CreateInstance<Spawn>();
                spawn = new Spawn(spawnName, spawnHealth, spawnCollider, spawnTexture, spawnTime, spawnEnemy);
                AssetDatabase.CreateAsset(spawn, "Assets/Resources/GameObjects/" + spawnName + ".asset");
                spawnName = null;
                spawnHealth = 0;
                spawnTime = 0;
                spawnTexture = null;
                spawnEnemy = null;
            }
        }
        //when fourth tab is selected
        if (toolBar == 3)
        {
            //fields for the items
            itemName = EditorGUILayout.TextField("Name:", itemName);
            itemHealth = EditorGUILayout.Slider("Health Value", itemHealth, 0, 100);
            itemPoints = EditorGUILayout.Slider("Points Value", itemPoints, 0, 100);
            itemCollider = (Colliders)EditorGUILayout.EnumPopup("Collider", itemCollider);
            itemTexture = (Sprite)EditorGUILayout.ObjectField("Item:", itemTexture, typeof(Sprite), true, GUILayout.MaxWidth(200));
            if (GUILayout.Button("Add Item"))
            {
                Item item = ScriptableObject.CreateInstance<Item>();
                item = new Item(itemName, itemHealth, itemCollider, itemTexture, itemPoints);
                AssetDatabase.CreateAsset(item, "Assets/Resources/GameObjects/" + itemName + ".asset");
                itemName = null;
                itemHealth = 0;
                itemPoints = 0;
                itemTexture = null;
            }
        }
        //when fivth tab is selected
        if (toolBar == 4)
        {
            //fields for the start
            startName = EditorGUILayout.TextField("Name:", spawnName);
            startCollider = (Colliders)EditorGUILayout.EnumPopup("Collider", startCollider);
            startTexture = (Sprite)EditorGUILayout.ObjectField("Start:", startTexture, typeof(Sprite), true, GUILayout.MaxWidth(200));
            if (GUILayout.Button("Add Start"))
            {
                Start start = ScriptableObject.CreateInstance<Start>();
                start = new Start(startName, startCollider, startTexture);
                AssetDatabase.CreateAsset(start, "Assets/Resources/GameObjects/" + startName + ".asset");
                startName = null;
                startTexture = null;
            }
        }
        //when sixth tab is selected
        if (toolBar == 5)
        {
            //field for the finish
            finishName = EditorGUILayout.TextField("Name:", finishName);
            endCollider = (Colliders)EditorGUILayout.EnumPopup("Collider", endCollider);
            finishTexture = (Sprite)EditorGUILayout.ObjectField("End:", finishTexture, typeof(Sprite), true, GUILayout.MaxWidth(200));
            if (GUILayout.Button("Add End"))
            {
                Finish finish = ScriptableObject.CreateInstance<Finish>();
                finish = new Finish(finishName, endCollider, finishTexture);
                AssetDatabase.CreateAsset(finish, "Assets/Resources/GameObjects/" + finishName + ".asset");
                finishName = null;
                finishTexture = null;
            }
        }
    }
}
