﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor.PackageManager;
using static GameObjectsEditor;


public class Finish : ScriptableObject
{
    public string Name;
    public Sprite texture;
    public Colliders collider;
    public Finish(string name, Colliders col, Sprite text)
    {
        Name = name;
        collider = col;
        texture = text;
    }
}
