﻿using SimpleJSON;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[System.Serializable]
//public class TextureVsPosition : Dictionary<Vector2Int, Texture> { }
//[System.Serializable]
//public class ScriptableVsPosition : Dictionary<Vector2Int, ScriptableObject> { }
//creating a menu item
[CreateAssetMenu(fileName = "Level-00", menuName = "Create/Level")]
[System.Serializable]

//creating a scriptable object

public class Level : ScriptableObject
{
    public string Name;
    [Range(0, 30)]
    public int rows;
    [Range(0, 45)]
    public int columns;
    //a Dictionary to store the scriptable objects
    public Dictionary<Vector2Int, ScriptableObject> LayerOne = new Dictionary<Vector2Int, ScriptableObject>();
    public Dictionary<Vector2Int, ScriptableObject> LayerTwo = new Dictionary<Vector2Int, ScriptableObject>();

    //to store the drawn cells in each layer
     public List<Vector2Int> layer1Cells=new List<Vector2Int>();
      public List<Vector2Int> layer2Cells=new List<Vector2Int>();

    //to store the respective texture of each cell
     public Dictionary<Vector2Int, Texture> Layer1 = new Dictionary<Vector2Int, Texture>();
    public Dictionary<Vector2Int, Texture> Layer2 = new Dictionary<Vector2Int, Texture>();

    //method to convert the current object in to a json
    public JSONNode serialize()
    {
        var n = new JSONObject();
        n["Name"] = Name;
        var h = n["GameObjects"].AsObject;
        foreach(var v in LayerOne)
        {
            if(v.Value.GetType() == typeof(Player))
            {
                Player player = (Player)v.Value;
                h[v.Key.ToString()] = player.ToString();
            }
        }
        foreach (var v in LayerTwo)
        {
            if (v.Value.GetType() == typeof(Player))
            {
                Player player = (Player)v.Value;
                h[v.Key.ToString()] = player.ToString();
            }
        }
        return n;
    }

}


